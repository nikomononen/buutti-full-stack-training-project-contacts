"use strict";
import fs from "fs";

/** 
 * Class to handle database
 */
class DatabaseHandler {
    /** @private */
    #contactsFile;

    /** @private */
    #groupsFile;
    
    /**
     * Constructs DatabaseHandler
     * @param {string} contactsFile - file where contacts is saved as JSON
     * @param {string} groupsFile - file where groups is saved as JSON
     */
    constructor(contactsFile, groupsFile){
        this.#contactsFile = contactsFile;
        this.#groupsFile = groupsFile;
    }

    /** 
     * Read contacts from database
     * @returns {Object} Contacts object
     */
    getContacts(){
        try {
            const data = fs.readFileSync(this.#contactsFile, {encoding:"utf8", flag:"r"});  
            return JSON.parse(data);
        } catch(err){
            return {};
        }
    }
    
    /** 
     * Read groups from database
     * @returns {Object} Groups object
     */
    getGroups(){
        try {
            const data = fs.readFileSync(this.#groupsFile, {encoding:"utf8", flag:"r"});  
            return JSON.parse(data);
        } catch(err){
            return {};
        }
    }

    /**
     * Save contacts to JSON file
     * @param {Object} objectData - Groups object to save
     */
    saveContacts(objectData){
        fs.writeFileSync(this.#contactsFile, JSON.stringify(objectData), {
            encoding: "utf8",
            flag: "w"
        });
    }

    /**
     * Save groups to JSON file
     * @param {Object} objectData - Groups object to save
     */
    saveGroups(objectData){
        fs.writeFileSync(this.#groupsFile, JSON.stringify(objectData), {
            encoding: "utf8",
            flag: "w"
        });
    }
}
export { DatabaseHandler };