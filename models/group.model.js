/**
 * Group 
 * @param {number} id - Unique id
 * @param {string} name - Contact first name
 * @returns {Group}
 */
const Group = function(index, name) {
    this.id = index;
    this.name = name;
    this.contacts = [];
};
export { Group };