# Project structure
```
- app.js (Console GUI)
- data (generated from given path strings, not to be added to Git repository)
    - contacts.json
    - groups.json
- services
    - database.service.js
    - contacts.service.js
- models (maybe)
    - contact.model.js
    - group.model.js
- ? tests (if time to do)
    - ? contacts.test.js
    ...
```
# Architecture
## Console UI
```
    - const app = new ContactsCommandLineUI("./data/contacts.json", "./data/groups.json")
    - help / instructions
    - input: readline
    - output: console.log()
    - parse input parameters
        - add -firstname Matti -lastname Meikäläinen -address Esimerkkikatu 12 A 1 -phone 123456789 ...
            - Quotes or without? eg. -address "Esimerkkikatu 1 A 2" OR from '-<param> ' to next ' -' or end of string
                - command is first word until " " and split rest with " -" for arguments and each argument with (command, value) = argument.split(" ");
            - Sort as parameter -sort <field> ASC|DESC
        // <id> is defined as immutable, it is used as selector
        - update -id <id> -firstname Teppo 
        - remove -id <id>
        - group-add -name Group name
        - group-update -id <id> -address <address>
        - group-remove -id <id>
        ...
        - search -firstname Teppo -sort ASC
        - search -firstname Teppo -sort lastname ASC
        - quit
            - Save contacts and groups to file as JSON. contacts.save();
```
### Input parsed <params> object to be used with method. 
Switch case for action and arguments object is given to the function as parameter
```
> add -firstname Teppo -lastname Testinen -phone 01234567689
{
    action: add,
    arguments: {
        firstname: "Teppo",
        lastname: "Testinen",
        phone: "0123456789"
    }
}
> update -id 12 -phone 01234567689
{
    action: update,
    arguments: {
        id: "12",
        phone: "0123456789"
    }
}
> search -firstname Teppo -sort lastname ASC
{
    action: search,
    arguments: {
        firstname: "Teppo",
        ...
        sort: {
            field: "lastname",
            order: "ASC"
        }
    }
}
> group-add -name Testiryhmä
{
    action: "group-add",
    arguments: {
        name: "Testiryhmä"
    }
}
```
## ContactsService / Class ("properly encapsulated" so private fields and methods are needed)
```
- ContactsService / Class
    - DatabaseHandler / Class / private
        - read() JSON.parse() ----------------------------------------+--+
        - save() JSON.stringify() -> contacts.json and groups.json    |  |
    - ContactsObject / Object / private  <----------------------------+  |
        - ?Contact / Class?Object                                        |
    - getContacts()                                                      |
    - addContact(<params>)                                               |
    - getContact(<id>)                                                   |
    - updateContact(<id>,<params>)                                       |
    - removeContact(<id>)                                                |
    - searchContact(<params>)                                            |  
    - GroupsObject / Object / private   <--------------------------------+
        - ?Group / Class?Object
    - getGroups()
    - getGroup(<id>)
    - addGroup(<params>)
    - addContactToGroup(<group-id>, <contact-id>)
    - removeContactFromGroup(<group-id>, <contact-id>)
    ...
    - ?extra functionality for later?
        - removeGroup(<id>)
        - searchGroup(<params>)
        - updateGroup(<id>)
```
### ContactsService class / Contacts - Object 
```
{
    1: { 
        id: 1, // Generated and added as key for the Contacts object entry
        firstname: "Teppo",
        lastname: "Testinen",
        address: "Testikatu 1 A 2"
    },
    2: {
        id: 2,
        firstname: "Matti",
        lastname: "Testinen",
        email: "matti.testinen@email.com",
    },
    3: {
        id: 3,
        firstname: "Seppo",
        lastname: "Testinen",
        address: "Testikatu 1 A 2"
        email: "seppo.testinen@email.com",
        phone: "0123456789",
        notes: "Tepon serkku"
    }
    ...
}
```
### ContactsService class / Groups - Object
```
{
    1: { 
        id: 1, // Generated and added as key for the Groups object entry
        name: "Testiset",
        contacts: [1, 2, 3]
    },
    2: {
        id: 2,
        name: "Another group",
        contacts: []
    }
    ...
}
```

# Testing
https://jestjs.io/docs/getting-started

Jest also provides functionality to report a test code coverage ratio in our project. 
Simply by using ‘jest —coverage’ we can get information about how well our tests cover the whole project.
