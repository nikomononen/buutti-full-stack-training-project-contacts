"use strict";
import readline from "readline";
import { DatabaseHandler } from "./services/database.service.js";
import { ContactsService } from "./services/contacts.service.js";

/**
 * Command line UI for a contacts address book
 */
class ContactsCommandLineUI {
    /**
     * ContactsService instance 
     * @private 
     */
    #contactsService;

    /** 
     * Constructs ContactsCommandLineUI
     * @param {string} contactsFile - Contacts JSON file name with path
     * @param {string} groupsFile - Groups JSON file name with path
     */
    constructor(contactsFile, groupsFile) {
        if (!this.#checkString(contactsFile)) throw new Error("Contacts file is not specified");
        if (!this.#checkString(groupsFile)) throw new Error("Groups file is not specified");
        this.#contactsService = new ContactsService(new DatabaseHandler(contactsFile, groupsFile));
    }

    /**
     * Checks if string is defined and at least one character long
     * @private
     * @param {string} string
     * @returns {boolean}
     */
    #checkString = function (string) {
        if (string === undefined) return false;
        return string.length > 1;
    };

    /**
     * Print instructions
     */
    #printHelp = function () {
        console.log("Commands:");
        console.log("   help Prints instructions");
        console.log("   quit Exits program");
        console.log("Contacts:");
        console.log("   list -sort? <field> <ASC/DESC>");
        console.log("   search -firstname <?> -lastname <?> -address <?> -phone <?> -email <?> -notes <?>");
        console.log("   add -firstname <firstname> -lastname <lastname> -address <address?> -phone <phone?> -email <email?> -notes <notes?>");
        console.log("   update -id <number> -firstname <firstname?> -lastname <lastname?> -address <address?> -phone <phone?> -email <email?> -notes <notes?>");
        console.log("   updateBatch -groupid <number> -firstname <firstname?> -lastname <lastname?> -address <address?> -phone <phone?> -email <email?> -notes <notes?>");
        console.log("   remove -id <number>");
        console.log("   getContact -id <number>");
        console.log("Groups:");
        console.log("   listGroups Lists all groups");
        console.log("   getGroup -id <number>");
        console.log("   addGroup -name <name>");
        console.log("   removeGroup -id <number>");
        console.log("   groupAdd -contact <id> -group <id>");
        console.log("   groupRemove -contact <id> -group <id>");
    };

    /**
     * Parses command and arguments from given string
     * @private
     * @param {string} string
     * @returns {Object|null} Returns Object with command and arguments or null if given string is not valid
     */
    #parseInput = function (string) {
        // FORMAT: "<command> -<argument> <value> ... -<argument> <value>"
        // eg.     "add -firstname Teppo -lastname Testinen";
        const [command, ...argumentArray] = string.split(" -");
        if (command === undefined || command.length < 1) return null;
        const argumentsObject = argumentArray.length > 0 ? new Object() : null;
        for (const argument of argumentArray) {
            const field = argument.substring(0, argument.indexOf(" "));
            const value = argument.substring(argument.indexOf(" ") + 1, argument.length);
            if (field.length < 1) continue;
            switch (field) {
                case "sort": {
                    const [sortField, sortOrder] = value.split(" ");
                    if (sortField === undefined || sortOrder === undefined) continue;
                    const sortObject = {
                        field: sortField,
                        order: sortOrder
                    };
                    argumentsObject[field] = sortObject;
                    break;
                }
                default: {
                    argumentsObject[field] = value;
                }
            }
        }
        return new Object({
            command: command,
            arguments: argumentsObject
        });
    };

    /**
     * Print out contact to console
     * @param {Contact} contact
     */
    #printContact = function(contact, padding=""){
        // Print out each key-value pair from each contact
        Object.entries(contact).forEach(keyAndValue => {
            const [key, value] = keyAndValue;
            console.log(`${padding}${key}: ${value}`);
        });
    };
    
    /**
         * Prints out given group
         * @param {Group} group
         */
    #printGroup = function(group) {        
        if (group === undefined || group == null) { 
            console.log("No group found.");
        }
        console.log(" ");
        console.log(`id: ${group.id}`);
        console.log(`name: ${group.name}`);
        if (group.contacts.length > 0 ){
            console.log("Contacts:");
            group.contacts.forEach(contactId =>{
                const contact = this.#contactsService.getContact(contactId);
                this.#printContact(contact, "   ");
            });
        }
        console.log(" ");
    };
    
    // List all contacts
    #listAllContacts = function(contactsArray) {        
        if (contactsArray === undefined || contactsArray.length < 1) { 
            console.log("No contacts found.");
            return null;
        }
        console.log(" ");
        console.log("Printing out all contacts.");
        // Go through each contactObject from contactsObject
        contactsArray.forEach(contactObject => {
            console.log(" ");
            this.#printContact(contactObject);
        });
        console.log(" ");
        console.log(`Printed (${contactsArray.length}) contacts.`);
        console.log(" ");
    };

    /**
     * Prints out given groups
     * @param Array.<{Group}> groups
     */
    #listAllGroups = function(groups) {        
        if (groups === undefined || groups.length < 1) { 
            console.log("No groups found.");
            return;
        }
        console.log(" ");
        console.log("Printing out all groups.");
        groups.forEach(group =>{
            console.log(" ");
            console.log(`id: ${group.id}`);
            console.log(`name: ${group.name}`);
        });
        console.log(" ");
        console.log(`Printed (${groups.length}) groups.`);
        console.log(" ");
    };

    /**
     * Start contacts app
     */
    start = function () {
        const consoleLineReader = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        this.#printHelp();
        consoleLineReader.on("line", (line) => {
            const input = this.#parseInput(line);
            if (input === null) return;
            switch (input.command) {
                case "add": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const contact = this.#contactsService.addContact(
                        input.arguments.firstname,
                        input.arguments.lastname,
                        input.arguments);
                    if (contact !== null){
                        console.log(" ");
                        this.#printContact(contact);
                        console.log(" ");
                    }
                    else {
                        console.log("Error happened when adding contact");
                    }
                    break;
                }             
                case "update": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const contact = this.#contactsService.updateContact(
                        input.arguments.id,
                        input.arguments);
                    if (contact !== null){
                        console.log(" ");
                        this.#printContact(contact);
                        console.log(" ");
                    }
                    else {
                        console.log("Error happened when updating contact");
                    }
                    break;
                }
                case "updateBatch": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const success = this.#contactsService.updateBatch(
                        input.arguments.groupid,
                        input.arguments);
                    if (success){
                        console.log("batchUpdate is success");
                    }
                    else {
                        console.log("Error happened when batchUpdating group");
                    }
                    break;
                }              
                case "addGroup": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const group = this.#contactsService.addGroup(input.arguments.name);
                    if(group !== null) {
                        console.log(`Added group "${group.name}" with id: ${group.id}`);
                    }
                    else {
                        console.log("Error happened when trying to add group");
                    }
                    break;
                }
                case "listGroups": {
                    const groups = this.#contactsService.getAllGroups();
                    this.#listAllGroups(groups);
                    break;
                }
                case "getContact": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const id = input.arguments.id;
                    const contact = this.#contactsService.getContact(id);
                    if (contact !== null) {
                        console.log(" ");
                        this.#printContact(contact);
                        console.log(" ");
                    } else {
                        console.log(`Error happened when trying to get contact id: ${id}`);
                    }
                    break;
                }
                case "getGroup": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const id = input.arguments.id;
                    const group = this.#contactsService.getGroup(id);
                    if (group !== null) {
                        this.#printGroup(group);
                    } else {
                        console.log(`Error happened when trying to get group id: ${id}`);
                    }
                    break;
                }
                case "removeGroup": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const id = input.arguments.id;
                    const success = this.#contactsService.removeGroup(id);
                    if(success) {
                        console.log(`Removed group with id: ${id}`);
                    }
                    else {
                        console.log("Error happened when trying to remove group");
                    }
                    break;
                }
                case "groupAdd": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const success = this.#contactsService.addContactToGroup(
                        input.arguments.contact, 
                        input.arguments.group
                    );
                    if(success) {
                        console.log(`Contact id: ${input.arguments.contact} successfully added to group id: ${input.arguments.group}`);
                    }
                    else {
                        console.log(`Error adding contact id: ${input.arguments.contact} to group id: ${input.arguments.group}`);
                    }
                    break;
                }
                case "groupRemove": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const success = this.#contactsService.removeContactFromGroup(
                        input.arguments.contact, 
                        input.arguments.group
                    );
                    if(success) {
                        console.log(`Contact id: ${input.arguments.contact} successfully removed from group id: ${input.arguments.group}`);
                    }
                    else {
                        console.log(`Error removing contact id: ${input.arguments.contact} from group id: ${input.arguments.group}`);
                    }
                    break;
                }
                case "help": {
                    this.#printHelp();
                    break;
                }
                case "list": {
                    if ((input.arguments !==  null) && !(Object.prototype.hasOwnProperty.call(input.arguments, "sort"))) {
                        console.log("Input is not valid. use 'help' to list available commands.");
                        break;
                    }
                    const allContacts = this.#contactsService.getAllContacts();
                    if (input.arguments === null) {
                        this.#listAllContacts(allContacts);
                    } else {
                        const sortField = input.arguments.sort.field;
                        const sortOrder = input.arguments.sort.order;
                        const sortedContacts = this.#contactsService.sortContacts(allContacts, sortField, sortOrder);
                        if (sortedContacts) {
                            this.#listAllContacts(sortedContacts);
                        }
                    }
                    break;
                }
                case "search": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const result = this.#contactsService.searchContact(input.arguments);
                    if (result.length === 0) console.log("No contacts found");
                    else {
                        result.forEach(contact => {
                            console.log("");
                            this.#printContact(contact);
                        });
                    }
                    break;
                }
                case "remove": {
                    if (input.arguments === null) {
                        console.log("Not enough arguments");
                        break;
                    }
                    const id = input.arguments.id;
                    const success = this.#contactsService.removeContact(id);
                    if(success) console.log(`Contact id: ${id} removed successfully`);
                    else console.log("Invalid request: id not found.");
                    break;
                }
                case "quit": {
                    consoleLineReader.close();
                    this.#contactsService.save();
                    break;
                }
                default: {
                    console.log("Input is not valid. use 'help' to list available commands.");
                }
            }
        });
    };
}

// Create app
const app = new ContactsCommandLineUI("./data/contacts.json", "./data/groups.json");
app.start();